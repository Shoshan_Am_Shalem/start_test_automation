import requests
import json
from http.client import HTTPSConnection
from requests.auth import HTTPBasicAuth
import mysql.connector
from datetime import datetime
import time
from calendar import timegm
import difflib
import MySQLdb
import collections
from collections import defaultdict
import subprocess
import os
import vlc

mydb = MySQLdb.connect(
host="104.196.181.106",
user="root",
passwd="Gabriel@2018",
database="gabriel"
    )

propertyId=20
mycursor = mydb.cursor()

print("DOES IT WORK???")
print("")

def CheckingIfThereIsEventOrNot():

    print("start checking")
    mycursor.execute('''
    select eventId from PrxEvent where status = 0 and propertyId = %s order by eventId desc''', ( propertyId, ))
    CheckingIfThereIsEventOrNot.TrueOrFalse = mycursor.fetchall()

    if CheckingIfThereIsEventOrNot.TrueOrFalse:
        print("There is an active event which I gonnat stop it")
    else:
        print("There is NO an active event so I'll open one")
    print("finish checking")
    return(CheckingIfThereIsEventOrNot.TrueOrFalse )

def Login():

    login_data =  {"email":"shoshan@gabriel.network", "password": "gabriel"}
    s= requests.Session()
    r = s.post('https://dev.gabriel.network/api/v1/login', json=login_data )

    json_data_post = json.loads(r.text)
    Login.cookies = r.cookies


    return (Login.cookies)

def APIStopEventCall():

    mycursor.execute("select eventId from PrxEvent where status =0 and propertyId= %s order by eventId desc limit 1", (propertyId,))

    for (item) in mycursor:
        EventId = item[0]

    stop_data ={"eventId": EventId }
    print(stop_data)
    resp = requests.post("https://dev.gabriel.network/api/v1/event/stop", json = stop_data, cookies = Login.cookies )
    json_data = json.loads(resp.text)

    #print(json_data.requestGuid)
    print("The Response of stop is" )
    print(resp)
    return

def APIStartEventCall(i):

    start_data = {"propertyId": propertyId,  "eventLevel": 0, "emergencyType": i, "initiatorClient": 2  }
    resp = requests.post("https://dev.gabriel.network/api/v1/event/start", json = start_data, cookies = Login.cookies )
    json_data = json.loads(resp.text)

    APIStartEventCall.requestGuid = json_data["requestGuid"]
    print("")
    print("The Response of start is" )
    print(resp)
    print(resp.content)
    return (APIStartEventCall.requestGuid)

def CalculateTime():

    print("Hi I entered to Calculate Time function")
    mycursor.execute('''SELECT timediff(b.insertTime, a.insertTime)
                        from LogRequest as a
                        join LogDetails as b
                        on a.requestGuid = b.requestGuid
                        where a.requestName = "/api/v1/event/start"
                        and b.message = "Did Create event"
                        order by a.insertTime desc limit 1
                        ''')
    for (item) in mycursor:
        CalculationTime = item[0]

    print(CalculationTime)
    return

def CheckAllNotifications():

    print("Starting Checking Notifications")
    mycursor.execute('''
            	 SELECT
                     SUBSTRING(gabriel.PrxUserDevice.token , 1 , 64) as token
                     FROM gabriel.PrxUser
                     left JOIN gabriel.PrxPropertyToUser
                     ON gabriel.PrxUser.userId = gabriel.PrxPropertyToUser.userId
                     right join gabriel.PrxUserDevice
                     ON gabriel.PrxUser.userId = gabriel.PrxUserDevice.userid
                     where gabriel.PrxPropertyToUser.propertyId = %s
                     AND gabriel.PrxUser.role in (0, 1, 2, 3)
                     ''',(propertyId,))

    ListOfTokenThatShouldBeSent = []
    for (item) in mycursor:
        ListOfTokenThatShouldBeSent.append(item[0])

    print(" ")
    print("APIStartEventCall.requestGuid:  ")
    print(APIStartEventCall.requestGuid)

    WillSendPushNotificationString ='%will send push to token:%'

    mydb.commit()


    ListOfTokenThatDidSent = []
    mycursor.execute ('''
                        select SUBSTRING(gabriel.LogDetails.message , 26, 64) as token
                        from gabriel.LogDetails
                        where gabriel.LogDetails.requestGuid = %s
                        and gabriel.LogDetails.message like %s
                        ''' , (APIStartEventCall.requestGuid, WillSendPushNotificationString ))
    print(mycursor._last_executed)
    #ListOfTokenThatDidSent = mycursor.fetchall()
    for (item) in mycursor:
        ListOfTokenThatDidSent.append(item[0])

    compare = lambda x, y: collections.Counter(x) == collections.Counter(y)
    print("The diff is: ")
    DeltaOfBotifications = compare(ListOfTokenThatShouldBeSent, ListOfTokenThatDidSent)
    if DeltaOfBotifications:
        print("Well done! all the tokens you wanted to send were sent!")
    else:
        print("Check you tokens again pleaes, not all tokens you wanted to send were sent eventually")

    return

def CheckDBTables():

    mycursor.execute("select eventId from PrxEvent where status =0 and PrxEvent.propertyId = %s", (propertyId,))
    for (item) in mycursor:
        EventTable = item[0]

    mydb.commit()

    mycursor.execute("SELECT eventId FROM gabriel.PrxEventTypeHistory order by eventId desc limit 1")
    for (item) in mycursor:
        EventHistoryTable = item[0]
    if EventTable == EventHistoryTable:
        print ("Fantastic job! All data was inserted to DB correctly")
    else:
        print ("You have a problem with inserting data to DB")


    return


def CheckEmergencyType():
    mydb.commit()
    APIStopEventCall()
    mydb.commit()
    EventLevel=[]
    for i in range(0,3):
        APIStartEventCall(i)
        print("The i is")
        print(i)
        print("")
        mydb.commit()
        mycursor.execute("select emergencyType from PrxEvent order by eventId desc limit 1")
        for (item) in mycursor:
            EventLevel.append(item[0])
        mydb.commit()
        APIStopEventCall()
        mydb.commit()


    print("The EventLevel array: ")
    print(EventLevel)
    print("")
    if EventLevel == [0, 1 ,2]:
        print ("Your emergencyType is as expected")
    else:
        print("There's a problem with your emergencyType")
    return

def StartDrill(i):

    start_data = {"propertyId": propertyId, "eventLevel": 1, "eventType": 2, "emergencyType": i , "initiatorClient": 2}

    resp = requests.post("https://dev.gabriel.network/api/v1/drill/start", json = start_data, cookies = Login.cookies )
    json_data = json.loads(resp.text)

    print("")
    print("The I is: ")
    print(i)
    print("")
    print("The Response of Drill start is" )
    print(resp)
    time.sleep(3)
    APIStopEventCall()
    mydb.commit()
    return

def CheckChangeEmergencyType():
    i=0
    for i in range(1,3):

        print("The I is :")
        print(i)

        if (i == 1):
    #     APIStartEventCall(0)
            ChangeEmergencyType=[]
            mycursor.execute("select emergencyType from PrxEvent order by eventId desc limit 1")
            for (item) in mycursor:
                ChangeEmergencyType.append(item[0])
        print(ChangeEmergencyType)
        mydb.commit()
        mycursor.execute("select eventId from PrxEvent where status =0 and propertyId=%s order by eventId desc limit 1",(propertyId,))
        for (item) in mycursor:
            print("line 80")
            EventId = item[0]
        mydb.commit()
        EventId = str(EventId)
        ChangeEmergencyTypeData =  {"emergencyType": i,  "eventLevel": 1}
        resp = requests.put("https://dev.gabriel.network/api/v1/event/" + EventId + "/emergencytype", json = ChangeEmergencyTypeData, cookies = Login.cookies )
    # json_data = json.loads(resp.text)

    #print(json_data.requestGuid)
        print("The Response of Change Emergency Type Data is" )
        print(resp)
        print(resp.content)

        mycursor.execute("select emergencyType from PrxEvent order by eventId desc limit 1")
        for (item) in mycursor:
            ChangeEmergencyType.append(item[0])
        mydb.commit()

        print("The ChangeEmergencyType array: ")
        print(ChangeEmergencyType)
        print("")

    if ChangeEmergencyType == [0, 1 ,2]:
        print ("Your emergencyType is as expected")
    else:
        print("There's a problem with your emergencyType")

    return

def CheckThatServerSentStream():
    CheckWhatCoordinatorsBelongedToEvent=[]
    mycursor.execute("SELECT PrxShield.shieldId FROM gabriel.PrxShield where propertyId = %s and type =1", (propertyId,))
    for (item) in mycursor:
        CheckWhatCoordinatorsBelongedToEvent.append(item[0])
    mydb.commit()
    print(CheckWhatCoordinatorsBelongedToEvent[0])
    CheckWhatCoordinatorsBelongedToEvent[0] = str(CheckWhatCoordinatorsBelongedToEvent[0])
    CheckThatServerSentStream=[]
    messageColumn ='%MQTT message did send to shield: '+ CheckWhatCoordinatorsBelongedToEvent[0] + '%'
    print(messageColumn)
    StartStream = '%startStream%'
    mycursor.execute('''
                    SELECT id
                    FROM gabriel.LogDetails
                    where message like %s
                    and message like %s
                    and requestGuid = %s ''', (messageColumn , StartStream ,APIStartEventCall.requestGuid))

    for (item) in mycursor:
        CheckThatServerSentStream.append(item[0])
    mydb.commit()

    CheckWhatCoordinatorsBelongedToEvent[1] = str(CheckWhatCoordinatorsBelongedToEvent[1])
    messageColumn ='%MQTT message did send to shield: '+ CheckWhatCoordinatorsBelongedToEvent[1] + '%'
    mycursor.execute('''
                    SELECT id
                    FROM gabriel.LogDetails where message
                    like %s
                    and message like %s
                    and requestGuid = %s ''', (messageColumn , StartStream ,APIStartEventCall.requestGuid))

    for (item) in mycursor:
        CheckThatServerSentStream.append(item[0])
    print(CheckThatServerSentStream[0])
    print(CheckThatServerSentStream[1])

    if ((CheckThatServerSentStream[0]) and (CheckThatServerSentStream[1])):
        print("Well done! sever sent stream")
    else:
        print("You have a problem sever DID NOT sent stream")

    return


def CheckVideoInVLC():
    mydb.commit()
    ShieldId=[]
    mycursor.execute("SELECT shieldId FROM gabriel.PrxShield where propertyId = 20")
    for (item) in mycursor:
        ShieldId.append(item[0])
    ShieldId=mycursor.fetchall()
    shieldId = str (shieldId)
    vlcInstance = vlc.Instance()
    for i in range(0,4):
        player = vlcInstance.media_player_new()
        player.set_mrl("rtsp://104.196.130.249:1935/gabriel_dev/" + shieldId[i] + ".stream_vp8")
        player.play()

    return

CheckingIfThereIsEventOrNot()
Login()
if CheckingIfThereIsEventOrNot.TrueOrFalse:
     APIStopEventCall()
     time.sleep(3)
if CheckingIfThereIsEventOrNot.TrueOrFalse:
     APIStopEventCall()
i=0
APIStartEventCall(i)
CalculateTime()
CheckAllNotifications()
CheckDBTables()
CheckEmergencyType()
for i in range(0,3):
    StartDrill(i)
APIStartEventCall(0)
CheckChangeEmergencyType()
CheckThatServerSentStream()
CheckVideoInVLC()
#def FakeTriggerUsingMQTT():

#    return

def MainFunction():
    APIStartEventCall()     #for all event types
    FakeTriggerUsingMQTT()
    CalculateTime()# could be inside function of both startingEvenet functions
    CheckAllNotifications()
    CheckDBTables() #might be with inside functions
    CheckEmergencyType()
    StartDrill()
    CheckChangeEmergencyType()
    CheckThatServerSentStream()
    #?OpenLogsInTerminalInOrderToCheckIfShieldStartedStream()
    CheckVideoInVLC()
    SetManyShieldsForEvent()
    SetManyUsersForEvent()

    return



print("I'm at the end of your script ")
